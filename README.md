# Lab 8 - Exploratory testing and UI

**Website chosen:** <https://godotengine.org/qa/>

## **Test 1:** Get Most viewed question

| No. | Action | Verdict | Comment |
| --- | --- | --- | --- |
| 1 |Go to <https://godotengine.org/qa/>  | ✅ |   |
| 2 |Click on "Questions" at the top  | ❔ | Reopens a very similar window but with sorting opions. Seems unnecessary.   |
| 3 |Select the "Most views" sort option  | ✅ |   |
| 4 |Select the first question  | ✅ |   |

## **Test 2:** Get Latest unanswered question

| No. | Action | Verdict | Comment |
| --- | --- | --- | --- |
| 1 |Go to <https://godotengine.org/qa/>  | ✅ |   |
| 2 |click on "Unanswered" at the top  | ✅ |    |
| 3 |Select the first question  | ✅ |   |

## **Test 3:** Get a specific question

| No. | Action | Verdict | Comment |
| --- | --- | --- | --- |
| 1 |Go to <https://godotengine.org/qa/>  | ✅ |   |
| 2 |Click on the search bar  | ✅ |    |
| 3 |Input "Can't reference any scene when using @export with a PackedScene typed array (Array[PackedScene])." | ✅ |   |
| 4 |Click the search icon | ✅ |   |
| 5 |Open first question | ❔ |  Showing other results even though an exact match was found |
