from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

# Initialize the WebDriver
driver = webdriver.Chrome()

# Navigate to the QA section of the Godot Engine website
driver.get("https://godotengine.org/qa/")

# Click on the "Questions" link at the top of the page
questions_link = WebDriverWait(driver, 10).until(
    EC.element_to_be_clickable((By.LINK_TEXT, "Questions"))
)
questions_link.click()

# Select the "Most views" sort option
sort_by_select = WebDriverWait(driver, 10).until(
    EC.element_to_be_clickable((By.LINK_TEXT, "Most views"))
)
sort_by_select.click()


# Select the first question in the list
first_question_link = WebDriverWait(driver, 10).until(
    EC.element_to_be_clickable((By.CSS_SELECTOR, ".qa-q-item-title"))
)
first_question_link.click()


time.sleep(5)

# Close the browser
driver.quit()
